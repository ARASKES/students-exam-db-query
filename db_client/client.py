import psycopg2
import time

time.sleep(30)

conn = psycopg2.connect(dbname='exam', user='postgres',
                        password='postgres', host='db', port=5432)
cursor = conn.cursor()

cursor.execute('select idBook, examDate from exam_schema.recordbook ORDER BY idBook;')
records = cursor.fetchall()

with open("output.txt", "w+") as file:
    for i in range(len(records)):
        print(f'{records[i][0]} {records[i][1]}')
        file.write(f'{records[i][0]} {records[i][1]}\n')

cursor.close()
conn.close()
